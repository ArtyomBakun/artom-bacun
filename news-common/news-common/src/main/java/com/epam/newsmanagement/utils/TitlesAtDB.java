package com.epam.newsmanagement.utils;

public class TitlesAtDB {
	public static final String AUTHOR_TABLE = "\"Author\"";
	public static final String AUTHOR_ID = "AUTHOR_ID";
	public static final String AUTHOR_NAME = "AUTHOR_NAME";
	public static final String AUTHOR_EXPIRED = "EXPIRED";
	
	public static final String NEWS_TABLE = "\"News\"";
	public static final String NEWS_ID = "NEWS_ID";
	public static final String TITLE = "TITLE";
	public static final String SHORT_TEXT = "SHORT_TEXT";
	public static final String FULL_TEXT = "FULL_TEXT";
	public static final String CREATION_DATE = "CREATION_DATE";
	public static final String MODIFICATION_DATE = "MODIFICATION_DATE";
	
	public static final String TAG_TABLE = "\"Tag\"";
	public static final String TAG_ID = "TAG_ID";
	public static final String TAG_NAME = "TAG_NAME";
	
	public static final String NEWS_TAG_TABLE = "\"News_Tag\"";
	public static final String NEWS_AUTHOR_TABLE = "\"News_Author\"";
	
	public static final String COMMENT_TABLE = "\"Comment\"";
	public static final String COMMENT_ID = "COMMENT_ID";
	public static final String COMMENT_TEXT = "COMMENT_TEXT";
	public static final String COMMENT_CREATION_DATE = "CREATION_DATE";

}
