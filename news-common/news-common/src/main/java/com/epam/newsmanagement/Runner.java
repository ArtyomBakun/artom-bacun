package com.epam.newsmanagement;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.dao.impl.DaoAuthorImpl;
import com.epam.newsmanagement.exception.DaoException;

public class Runner {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Contex.xml");
		DataSource dataSource = (DataSource)context.getBean("dataSource");
		DaoAuthorImpl dai = new DaoAuthorImpl(dataSource);
//		Author a = new Author();
//		a.setName("Victor");
//		System.out.println(dai.read(3));
//		dai.create(a);
//		System.out.println(dai.getAllAuthors());
//		a.setAuthorId(6);
//		dai.update(a);
//		System.out.println(dai.getAllAuthors());
//		List<Long> ids = new ArrayList<Long>();
//		ids.add(6l);
//		ids.add(7l);
//		dai.deleteBatch(ids);
		try {
			System.out.println(dai.getAllAuthors());
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}

}
