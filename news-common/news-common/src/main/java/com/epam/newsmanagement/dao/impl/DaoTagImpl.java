package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.DaoTag;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

import static com.epam.newsmanagement.utils.Queries.*;
import static com.epam.newsmanagement.utils.TitlesAtDB.*;
import static com.epam.newsmanagement.utils.Messages.*;

public class DaoTagImpl implements DaoTag {

	private DataSource dataSource;

	public DaoTagImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public boolean create(Tag entity) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psCreate = (PreparedStatement) conn.prepareStatement(INSERT_TAG)) {
			conn.setAutoCommit(false);
			psCreate.setString(1, entity.getTagName());
			if (psCreate.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public Tag read(long id) throws DaoException {
		Tag tag = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psRead = (PreparedStatement) conn.prepareStatement(GET_TAG_BY_ID)) {
			psRead.setLong(1, id);
			ResultSet rs = psRead.executeQuery();
			if (rs.next()) {
				tag = mappingDataForObject(rs);
			}
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return tag;
	}

	@Override
	public boolean update(Tag entity) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psUpdate = (PreparedStatement) conn.prepareStatement(UPDATE_TAG)) {
			conn.setAutoCommit(false);
			psUpdate.setString(1, entity.getTagName());
			psUpdate.setLong(2, entity.getTagId());
			if (psUpdate.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public boolean delete(long id) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psDeleteChildRecords = (PreparedStatement) conn.prepareStatement(DELETE_TAG_NEWS);
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_TAG)) {
			conn.setAutoCommit(false);
			psDeleteChildRecords.setLong(1, id);
			psDeleteChildRecords.executeUpdate();//remove records of this tag from News_Tag table
			psDelete.setLong(1, id);
			if (psDelete.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public boolean deleteBatch(List<Long> ids) throws DaoException {
		boolean rez = true;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_TAG)) {
			conn.setAutoCommit(false);
			for (Long id : ids) {
				psDelete.setLong(1, id);
				if (psDelete.executeUpdate() != 1) {
					rez = false;
					break;
				}
			}
			if (rez) {
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public List<Tag> getAllTags() throws DaoException {
		List<Tag> tags = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psGetAll = (PreparedStatement) conn.prepareStatement(GET_ALL_TAGS)) {
			ResultSet rs = psGetAll.executeQuery();
			if (rs.next()) {
				tags = new ArrayList<Tag>();
			}
			do {
				tags.add(mappingDataForObject(rs));
			} while (rs.next());
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return tags;
	}

	@Override
	public List<Tag> getAllTagsForOneNew(long newId) throws DaoException {
		List<Tag> tags = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psGetAll = (PreparedStatement) conn.prepareStatement(GET_ALL_TAGS_FOR_ONE_NEW)) {
			ResultSet rs = psGetAll.executeQuery();
			if (rs.next()) {
				tags = new ArrayList<Tag>();
			}
			do {
				tags.add(mappingDataForObject(rs));
			} while (rs.next());
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return tags;
	}

	@Override
	public List<Tag> getAllTagsExcludeOneNew(long newId) throws DaoException {
		List<Tag> tags = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psGetAll = (PreparedStatement) conn.prepareStatement(GET_ALL_TAGS_EXCLUDE_ONE_NEW)) {
			ResultSet rs = psGetAll.executeQuery();
			if (rs.next()) {
				tags = new ArrayList<Tag>();
			}
			do {
				tags.add(mappingDataForObject(rs));
			} while (rs.next());
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return tags;
	}

	private Tag mappingDataForObject(ResultSet rs) throws SQLException {
		Tag tag = new Tag();
		tag.setTagId(rs.getLong(TAG_ID));
		tag.setTagName(rs.getString(TAG_NAME));
		return tag;
	}

}
