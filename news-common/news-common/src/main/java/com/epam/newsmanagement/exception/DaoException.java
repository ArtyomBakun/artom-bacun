package com.epam.newsmanagement.exception;

public class DaoException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2267865649479169633L;
	
	private Exception cause;
	private String message;
	
	public DaoException(Exception cause, String message) {
		this.cause = cause;
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	@Override
	public synchronized Throwable getCause() {
		return cause;
	}
	
	@Override
	public void printStackTrace() {
		super.printStackTrace();
	}

}
