package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.DaoNews;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DaoException;

import static com.epam.newsmanagement.utils.Queries.*;
import static com.epam.newsmanagement.utils.TitlesAtDB.*;
import static com.epam.newsmanagement.utils.Messages.*;

public class DaoNewsImpl implements DaoNews {

	private DataSource dataSource;

	public DaoNewsImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public boolean create(News entity) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psCreate = (PreparedStatement) conn.prepareStatement(INSERT_NEWS)) {
			conn.setAutoCommit(false);
			psCreate.setString(1, entity.getTitle());
			psCreate.setString(2, entity.getShortText());
			psCreate.setString(3, entity.getFullText());
			psCreate.setTimestamp(4, entity.getCreationDate());
			psCreate.setDate(5, entity.getModificationDate());
			if (psCreate.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public News read(long id) throws DaoException {
		News news = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psRead = (PreparedStatement) conn.prepareStatement(GET_NEWS_BY_ID)) {
			psRead.setLong(1, id);
			ResultSet rs = psRead.executeQuery();
			if (rs.next()) {
				news = mappingDataForObject(rs);
			}
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return news;
	}

	@Override
	public boolean update(News entity) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psUpdate = (PreparedStatement) conn.prepareStatement(UPDATE_NEWS)) {
			conn.setAutoCommit(false);
			psUpdate.setString(1, entity.getTitle());
			psUpdate.setString(2, entity.getShortText());
			psUpdate.setString(3, entity.getFullText());
			psUpdate.setTimestamp(4, entity.getCreationDate());
			psUpdate.setDate(5, entity.getModificationDate());
			psUpdate.setLong(6, entity.getNewsId());
			if (psUpdate.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public boolean delete(long id) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_NEWS)) {
			conn.setAutoCommit(false);
			psDelete.setLong(1, id);
			if (psDelete.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public boolean deleteBatch(List<Long> ids) throws DaoException {
		boolean rez = true;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_NEWS)) {
			conn.setAutoCommit(false);
			for (Long id : ids) {
				psDelete.setLong(1, id);
				if (psDelete.executeUpdate() != 1) {
					rez = false;
					break;
				}
			}
			if (rez) {
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public List<News> getAllNews() throws DaoException {
		List<News> news = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psGetAll = (PreparedStatement) conn.prepareStatement(GET_ALL_NEWS)) {
			ResultSet rs = psGetAll.executeQuery();
			if (rs.next()) {
				news = new ArrayList<News>();
			}
			do {
				news.add(mappingDataForObject(rs));
			} while (rs.next());
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return news;
	}

	@Override
	public boolean addTags(long newsId, List<Long> tagIds) throws DaoException {
		boolean rez = true;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psAddTag = (PreparedStatement) conn.prepareStatement(ADD_NEWS_TAG)) {
			conn.setAutoCommit(false);
			psAddTag.setLong(1, newsId);
			for (Long id : tagIds) {
				psAddTag.setLong(2, id);
				if (psAddTag.executeUpdate() != 1) {
					rez = false;
					break;
				}
			}
			if (rez) {
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	private News mappingDataForObject(ResultSet rs) throws SQLException {
		News news = new News();
		news.setNewsId(rs.getLong(NEWS_ID));
		news.setTitle(rs.getString(TITLE));
		news.setShortText(rs.getString(SHORT_TEXT));
		news.setFullText(rs.getString(FULL_TEXT));
		news.setCreationDate(rs.getTimestamp(CREATION_DATE));
		news.setModificationDate(rs.getDate(MODIFICATION_DATE));
		return news;
	}

	@Override
	public int totalNewsNumber() throws DaoException {
		int total = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psGetTotalNumber = (PreparedStatement) conn.prepareStatement(TOTAL_NEWS_COUNT)) {
			ResultSet rs = psGetTotalNumber.executeQuery();
			rs.next();
			total = rs.getInt(1);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return total;
	}

	@Override
	public boolean deleteTag(long tagId) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_NEWS_TAG)) {
			conn.setAutoCommit(false);
			psDelete.setLong(1, tagId);
			if (psDelete.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public long getAuthorByNews(int newsId) throws DaoException {
		int authorID = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psGetTotalNumber = (PreparedStatement) conn.prepareStatement(GET_AUTHOR_BY_NEWS)) {
			psGetTotalNumber.setLong(1, newsId);
			ResultSet rs = psGetTotalNumber.executeQuery();
			rs.next();
			authorID = rs.getInt(1);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return authorID;
	}

}
