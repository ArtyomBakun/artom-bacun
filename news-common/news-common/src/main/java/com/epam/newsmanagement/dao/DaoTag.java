package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DaoException;

public interface DaoTag extends DAO<Tag> {
	
	List<Tag> getAllTags() throws DaoException;
	
	List<Tag> getAllTagsForOneNew(long newId) throws DaoException;
	
	List<Tag> getAllTagsExcludeOneNew(long newId) throws DaoException;
	
}
