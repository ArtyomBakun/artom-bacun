package com.epam.newsmanagement.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.DaoAuthor;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.epam.newsmanagement.utils.Queries.*;
import static com.epam.newsmanagement.utils.TitlesAtDB.*;
import static com.epam.newsmanagement.utils.Messages.*;

public class DaoAuthorImpl implements DaoAuthor{
	
	private DataSource dataSource;
	
	public DaoAuthorImpl(DataSource dataSource){
		this.dataSource = dataSource;
	}

	public boolean create(Author entity) throws DaoException {
		boolean rez = false;
		try(Connection conn = dataSource.getConnection();
				PreparedStatement psCreate = (PreparedStatement) conn.prepareStatement(INSERT_AUTHOR)) {
			conn.setAutoCommit(false);
			psCreate.setString(1, entity.getName());
			psCreate.setTimestamp(2, entity.getExpiried());
			if (psCreate.executeUpdate()==1) {
				conn.commit();
				rez = true;
			}else{
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	public Author read(long id) throws DaoException {
		Author author = null;
		try(Connection conn = dataSource.getConnection();
				PreparedStatement psRead = (PreparedStatement) conn.prepareStatement(GET_AUTHOR_BY_ID)) {
			psRead.setLong(1, id);
			ResultSet rs = psRead.executeQuery();
			if (rs.next()) {
				author = mappingDataForObject(rs);
			}
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return author;
	}

	public boolean update(Author entity) throws DaoException {
		boolean rez = false;
		try(Connection conn = dataSource.getConnection();
				PreparedStatement psUpdate = (PreparedStatement) conn.prepareStatement(UPDATE_AUTHOR)) {
			conn.setAutoCommit(false);
			psUpdate.setString(1, entity.getName());
			psUpdate.setTimestamp(2, entity.getExpiried());
			psUpdate.setLong(3, entity.getAuthorId());
			if (psUpdate.executeUpdate()==1) {
				conn.commit();
				rez = true;
			}else{
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	public boolean delete(long id) throws DaoException {
		boolean rez = false;
		try(Connection conn = dataSource.getConnection();
				PreparedStatement psDeleteChildRecords = (PreparedStatement) conn.prepareStatement(DELETE_AUTHOR_NEWS);
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_AUTHOR)) {
			conn.setAutoCommit(false);
			psDeleteChildRecords.setLong(1, id);
			psDeleteChildRecords.executeUpdate();
			psDelete.setLong(1, id);
			if (psDelete.executeUpdate()==1) {
				conn.commit();
				rez = true;
			}else{
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	public boolean deleteBatch(List<Long> ids) throws DaoException {
		boolean rez = true;
		try(Connection conn = dataSource.getConnection();
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_AUTHOR)) {
			conn.setAutoCommit(false);
			for (Long id : ids) {
				psDelete.setLong(1, id);
				if (psDelete.executeUpdate()!=1) {
					rez = false;
					break;
				}
			}
			if (rez) {
				conn.commit();
			}else{
				conn.rollback();
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	public List<Author> getAllAuthors() throws DaoException {
		List<Author> authors = null;
		try(Connection conn = dataSource.getConnection();
				PreparedStatement psGetAll = (PreparedStatement) conn.prepareStatement(GET_ALL_AUTHORS)) {
			ResultSet rs = psGetAll.executeQuery();
			if(rs.next()){
				authors = new ArrayList<Author>();
			}
			do {
				authors.add(mappingDataForObject(rs));
			}while (rs.next());
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return authors;
	}

	@Override
	public boolean addNews(long authorId, long newsId) throws DaoException {
		boolean rez = false;
		try(Connection conn = dataSource.getConnection();
				PreparedStatement psAddNews = (PreparedStatement) conn.prepareStatement(ADD_NEWS)) {
			conn.setAutoCommit(false);
			psAddNews.setLong(1, newsId);
			psAddNews.setLong(2, authorId);
			if (psAddNews.executeUpdate()==1) {
				conn.commit();
				rez = true;
			}else{
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}
	
	private Author mappingDataForObject(ResultSet rs) throws SQLException{
		Author author = new Author();
		author.setAuthorId(rs.getLong(AUTHOR_ID));
		author.setName(rs.getString(AUTHOR_NAME));
		author.setExpiried(rs.getTimestamp(AUTHOR_EXPIRED));
		return author;
	}
	
}
