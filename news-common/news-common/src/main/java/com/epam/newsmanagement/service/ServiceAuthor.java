package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;

public interface ServiceAuthor {
	
	Author createAuthor(Author a);
	boolean deleteAuthor(Author a);
	List<Author> getAllAuthors();

}
