package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DaoException;

public interface DaoAuthor extends DAO<Author>{
	
	List<Author> getAllAuthors() throws DaoException;
	
	boolean addNews(long authorId, long newsId) throws DaoException;
	
}
