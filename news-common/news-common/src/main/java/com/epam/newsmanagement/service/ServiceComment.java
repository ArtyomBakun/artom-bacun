package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;

public interface ServiceComment {
	
	boolean addComment(Comment c);
	
	boolean deleteComment(Comment c);
	
	List<Comment> getAllForOneNew(long  newId);
	
	int totalCommentsNumberForOneNews(long newsId);

}
