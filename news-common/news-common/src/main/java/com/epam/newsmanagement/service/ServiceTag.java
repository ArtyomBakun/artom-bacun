package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;

public interface ServiceTag {
	
	boolean cteateTag(Tag t);
	
	List<Tag> getAllTags();
	
	List<Tag> getAllTagsForOneNew(long newId);
	
	List<Tag> getAllTagsExcludeOneNew(long newId);

}
