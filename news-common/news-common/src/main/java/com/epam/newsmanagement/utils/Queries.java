package com.epam.newsmanagement.utils;

import static com.epam.newsmanagement.utils.TitlesAtDB.*;

public class Queries {

	// Author-----------------------------------
	public static final String INSERT_AUTHOR = "INSERT INTO " + AUTHOR_TABLE + " (" + AUTHOR_NAME + ", " + AUTHOR_EXPIRED
			+ ") VALUES (?, ?)";

	public static final String UPDATE_AUTHOR = "UPDATE " + AUTHOR_TABLE + " SET " + AUTHOR_NAME + " = ?, " + AUTHOR_EXPIRED
			+ " = ? WHERE " + AUTHOR_ID + " = ?";

	public static final String DELETE_AUTHOR = "DELETE FROM " + AUTHOR_TABLE + " WHERE " + AUTHOR_ID + " = ?";

	public static final String DELETE_AUTHOR_NEWS = "DELETE FROM " + NEWS_AUTHOR_TABLE + " WHERE " + AUTHOR_ID + " = ?";

	public static final String GET_AUTHOR_BY_ID = "SELECT * FROM " + AUTHOR_TABLE + " WHERE " + AUTHOR_ID + " = ?";

	public static final String GET_ALL_AUTHORS = "SELECT * FROM " + AUTHOR_TABLE;

	public static final String ADD_NEWS = "INSERT INTO " + AUTHOR_TABLE + " (" + NEWS_ID + " , " + AUTHOR_ID + ") VALUES (?, ?)";

	// News-------------------------------------
	public static final String INSERT_NEWS = "INSERT INTO " + NEWS_TABLE + " (" + TITLE + ", " + SHORT_TEXT + ", " + FULL_TEXT
			+ ", " + CREATION_DATE + ", " + MODIFICATION_DATE + ") VALUES (?, ?, ?, ?, ?)";

	public static final String GET_NEWS_BY_ID = "SELECT * FROM " + NEWS_TABLE + " WHERE " + NEWS_ID + " = ?";

	public static final String GET_ALL_NEWS = "SELECT * FROM " + NEWS_TABLE;

	public static final String UPDATE_NEWS = "UPDATE " + NEWS_TABLE + " SET " + TITLE + " = ?, " + SHORT_TEXT + " = ?, "
			+ FULL_TEXT + " = ?, " + CREATION_DATE + " = ?, " + MODIFICATION_DATE + " = ? WHERE " + NEWS_ID + " = ?";

	public static final String DELETE_NEWS = "DELETE FROM " + NEWS_TABLE + " WHERE " + NEWS_ID + " = ?";

	public static final String ADD_NEWS_TAG = "INSERT INTO " + NEWS_TAG_TABLE + " (" + NEWS_ID + " , " + TAG_ID
			+ ") VALUES (?, ?)";

	public static final String DELETE_NEWS_TAG = "DELETE FROM " + NEWS_TAG_TABLE + " WHERE " + NEWS_ID + " = ?, " + TAG_ID
			+ " = ?) VALUES (?, ?)";

	public static final String TOTAL_NEWS_COUNT = "SELECT COUNT(" + NEWS_ID + ") FROM " + NEWS_TABLE;

	public static final String GET_AUTHOR_BY_NEWS = "SELECT " + AUTHOR_ID + " FROM " + NEWS_AUTHOR_TABLE + " WHERE " + NEWS_ID
			+ " = ?";

	// Tag--------------------------------------
	public static final String INSERT_TAG = "INSERT INTO " + TAG_TABLE + " (" + TAG_NAME + ") VALUES (?)";

	public static final String GET_TAG_BY_ID = "SELECT * FROM " + TAG_TABLE + " WHERE " + TAG_ID + " = ?";

	public static final String UPDATE_TAG = "UPDATE " + TAG_TABLE + " SET " + TAG_NAME + " = ? WHERE " + TAG_ID + " = ?";

	public static final String DELETE_TAG = "DELETE FROM " + TAG_TABLE + " WHERE " + TAG_ID + " = ?";

	public static final String DELETE_TAG_NEWS = "DELETE FROM " + NEWS_TAG_TABLE + " WHERE " + TAG_ID + " = ?";

	public static final String GET_ALL_TAGS = "SELECT * FROM " + TAG_TABLE;

	public static final String GET_ALL_TAGS_FOR_ONE_NEW = "SELECT " + TAG_TABLE + "." + TAG_ID + ", " + TAG_NAME + " FROM "
			+ TAG_TABLE + "JOIN " + NEWS_TAG_TABLE + " ON " + NEWS_TAG_TABLE + "." + TAG_ID + " = " + TAG_TABLE + "." + TAG_ID
			+ " WHERE NEWS_ID = ?";

	public static final String GET_ALL_TAGS_EXCLUDE_ONE_NEW = "SELECT " + TAG_TABLE + "." + TAG_ID + ", " + TAG_NAME + " FROM "
			+ TAG_TABLE + "JOIN " + NEWS_TAG_TABLE + " ON " + NEWS_TAG_TABLE + "." + TAG_ID + " = " + TAG_TABLE + "." + TAG_ID
			+ " WHERE NEWS_ID <> ?";

	// Comment----------------------------------
	public static final String INSERT_COMMENT = "INSERT INTO " + COMMENT_TABLE + " (" + NEWS_ID + ", " + COMMENT_TEXT + ", "
			+ COMMENT_CREATION_DATE + ") VALUES (?, ?, ?))";

	public static final String GET_COMMENT_BY_ID = "SELECT * FROM " + COMMENT_TABLE + " WHERE " + COMMENT_ID + " = ?";

	public static final String GET_ALL_COMMENTS_FOR_ONE_NEWS = "SELECT * FROM " + COMMENT_TABLE + " WHERE " + NEWS_ID + " = ?";

	public static final String UPDATE_COMMENT = "UPDATE " + COMMENT_TABLE + " SET " + COMMENT_TEXT + " = ?  WHERE " + COMMENT_ID
			+ " = ?";

	public static final String DELETE_COMMENT = "DELETE FROM " + COMMENT_TABLE + " WHERE " + COMMENT_ID + " = ?";

	public static final String GET_NUMBER_COMMENTS_FOR_ONE_NEWS = "SELECT COUNT(" + COMMENT_ID + ") FROM " + COMMENT_TABLE
			+ " WHERE " + NEWS_ID + " = ?";

}
