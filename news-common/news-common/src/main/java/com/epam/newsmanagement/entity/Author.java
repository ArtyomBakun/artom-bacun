package com.epam.newsmanagement.entity;

import java.sql.Timestamp;

public class Author implements Entity {
	private long authorId;
	private String name;
	private Timestamp expiried;
	
	public long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Timestamp getExpiried() {
		return expiried;
	}

	public void setExpiried(Timestamp expiried) {
		this.expiried = expiried;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (authorId ^ (authorId >>> 32));
		result = prime * result + ((expiried == null) ? 0 : expiried.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId != other.authorId)
			return false;
		if (expiried == null) {
			if (other.expiried != null)
				return false;
		} else if (!expiried.equals(other.expiried))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "\nAuthor:\nauthorId=" + authorId + "\nname=" + name + "\nexpiried=" + expiried + ";";
	}


}
