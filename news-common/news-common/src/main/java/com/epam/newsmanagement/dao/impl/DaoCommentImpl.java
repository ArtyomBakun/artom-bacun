package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.DaoComment;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

import static com.epam.newsmanagement.utils.Queries.*;
import static com.epam.newsmanagement.utils.TitlesAtDB.*;
import static com.epam.newsmanagement.utils.Messages.*;

public class DaoCommentImpl implements DaoComment {

	private DataSource dataSource;

	public DaoCommentImpl(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public boolean create(Comment entity) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psCreate = (PreparedStatement) conn.prepareStatement(INSERT_COMMENT)) {
			conn.setAutoCommit(false);
			psCreate.setLong(1, entity.getNewsId());
			psCreate.setString(2, entity.getCommentText());
			psCreate.setTimestamp(3, entity.getCreationDate());
			if (psCreate.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public Comment read(long id) throws DaoException {
		Comment comment = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psRead = (PreparedStatement) conn.prepareStatement(GET_COMMENT_BY_ID)) {
			psRead.setLong(1, id);
			ResultSet rs = psRead.executeQuery();
			if (rs.next()) {
				comment = mappingDataForObject(rs);
			}
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return comment;
	}

	@Override
	public boolean update(Comment entity) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psUpdate = (PreparedStatement) conn.prepareStatement(UPDATE_COMMENT)) {
			conn.setAutoCommit(false);
			psUpdate.setString(1, entity.getCommentText());
			psUpdate.setLong(2, entity.getCommentId());
			if (psUpdate.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public boolean delete(long id) throws DaoException {
		boolean rez = false;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_COMMENT)) {
			conn.setAutoCommit(false);
			psDelete.setLong(1, id);
			if (psDelete.executeUpdate() == 1) {
				conn.commit();
				rez = true;
			} else {
				conn.rollback();
				rez = false;
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public boolean deleteBatch(List<Long> ids) throws DaoException {
		boolean rez = true;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psDelete = (PreparedStatement) conn.prepareStatement(DELETE_COMMENT)) {
			conn.setAutoCommit(false);
			for (Long id : ids) {
				psDelete.setLong(1, id);
				if (psDelete.executeUpdate() != 1) {
					rez = false;
					break;
				}
			}
			if (rez) {
				conn.commit();
			} else {
				conn.rollback();
			}
			conn.setAutoCommit(true);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return rez;
	}

	@Override
	public List<Comment> getAllForOneNew(long newId) throws DaoException {
		List<Comment> comments = null;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psGetAll = (PreparedStatement) conn.prepareStatement(GET_ALL_COMMENTS_FOR_ONE_NEWS)) {
			ResultSet rs = psGetAll.executeQuery();
			if (rs.next()) {
				comments = new ArrayList<Comment>();
			}
			do {
				comments.add(mappingDataForObject(rs));
			} while (rs.next());
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return comments;
	}

	@Override
	public int totalCommentsNumberForOneNews(long newsId) throws DaoException {
		int total = 0;
		try (Connection conn = dataSource.getConnection();
				PreparedStatement psGetTotalNumber = (PreparedStatement) conn
						.prepareStatement(GET_NUMBER_COMMENTS_FOR_ONE_NEWS)) {
			ResultSet rs = psGetTotalNumber.executeQuery();
			rs.next();
			total = rs.getInt(1);
		} catch (SQLException e) {
			throw new DaoException(e, SQL_EXCEPTION_ERROR_MESSAGE);
		}
		return total;
	}

	private Comment mappingDataForObject(ResultSet rs) throws SQLException {
		Comment comment = new Comment();
		comment.setCommentId(rs.getLong(COMMENT_ID));
		comment.setNewsId(rs.getLong(NEWS_ID));
		comment.setCommentText(rs.getString(COMMENT_TEXT));
		comment.setCreationDate(rs.getTimestamp(COMMENT_CREATION_DATE));
		return comment;
	}

}
