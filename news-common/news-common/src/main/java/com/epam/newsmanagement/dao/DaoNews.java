package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DaoException;

public interface DaoNews extends DAO<News> {
	
	List<News> getAllNews() throws DaoException;
	
	boolean addTags(long newsId, List<Long> tagIds) throws DaoException;
	
	boolean deleteTag(long tagId) throws DaoException;
	
	int totalNewsNumber() throws DaoException;
	
	long getAuthorByNews(int newsId) throws DaoException;
	
}
