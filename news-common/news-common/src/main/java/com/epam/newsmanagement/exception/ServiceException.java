package com.epam.newsmanagement.exception;

public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5024299878309494437L;
	
	private Exception cause;
	private String message;
	
	public ServiceException(Exception cause, String message) {
		this.cause = cause;
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	@Override
	public synchronized Throwable getCause() {
		return cause;
	}
	
	@Override
	public void printStackTrace() {
		super.printStackTrace();
	}

}
