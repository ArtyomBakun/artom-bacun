package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Entity;
import com.epam.newsmanagement.exception.DaoException;

public interface DAO<T extends Entity> {
	
	boolean create(T entity) throws DaoException;
	T read(long id) throws DaoException;
	boolean update(T entity) throws DaoException;
	boolean delete(long id) throws DaoException;
	boolean deleteBatch(List<Long> ids) throws DaoException;
}
