package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DaoException;

public interface DaoComment extends DAO<Comment> {
	
	List<Comment> getAllForOneNew(long  newId) throws DaoException;
	
	int totalCommentsNumberForOneNews(long newsId) throws DaoException;

}
